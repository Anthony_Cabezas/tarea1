<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
</head>

<body>
    <form name="login" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">

        <input type="text" name="nombre" id="nombre" placeholder="Nombre:">
        <br>

        <input type="text" name="apellido" id="apellido" placeholder="Apellido:">
        <br>

        <input type="text" name="telefono" id="telefono" placeholder="Telefono:">
        <br>

        <input type="email" name="correo" id="correo" placeholder="Correo:">
        <br>
        <input type="submit" name="submit"  value="Enviar Datos">
        <br>

        <?php if (!empty($errores)) : ?>
        <div>
            <ul><?php echo $errores; ?></ul>
        </div>
        <?php endif; ?>
        <br>

    </form>
</body>

</html>