<?php

$errores = '';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $nombre = trim($_POST['nombre']);
    $apellido = $_POST['apellido'];
    $telefono = $_POST['telefono'];
    $correo = $_POST['correo'];


    //verificacion de datos
	if(empty($nombre) or empty($apellido) or empty($telefono) or empty($correo)){
        $errores .= '<li>Por favor rellena todos los datos correctamente</li>';
    }

	//si no hay errrores realizamos la conexion y enviamos los datos a la base
	if ($errores == '') {
        try {
            $conexion = new PDO('mysql:host=localhost;dbname=tarea1', 'root', '');
        } catch (PDOException $e) {
            echo "Error: ". $e->getMessage();
        }

        $statement = $conexion->prepare('INSERT INTO usuarios (ID, nombre, apellido, telefono, correo) VALUES (null, :nombre, :apellido, :telefono, :correo)');
		$statement->execute(array(':nombre' => $nombre,':apellido' => $apellido, ':telefono' => $telefono, ':correo'=> $correo));
		$errores .=  '<li>Enviado correctamente</li>';
        header('Location: index.php');
	}
}

require 'view/index.view.php';